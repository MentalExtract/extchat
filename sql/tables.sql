CREATE TABLE IF NOT EXISTS `channel` (
  `name` varchar(16) NOT NULL,
  `description` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `channel` varchar(16) DEFAULT NULL,
  `user` varchar(16) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=545 ;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `lastFetch` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `usersinchannel` (
  `userName` varchar(32) NOT NULL,
  `channelName` varchar(16) NOT NULL,
  PRIMARY KEY (`userName`,`channelName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `friends` (
  `user` varchar(64) NOT NULL,
  `friendUser` varchar(64) NOT NULL,
  PRIMARY KEY (`user`,`friendUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
