<?php 
session_start();
include("config/defines.php");

if(!isset($_SESSION['isLogin'])){
	$_SESSION['isLogin'] = FALSE;
}
?>
<html>
<head>
<style>
html { 
	background: url(images/bgl.png) no-repeat center center fixed; 
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>
<script type="text/javascript">
var currentUserName = '<?php  if(isset($_SESSION['userName'])) { echo $_SESSION['userName']; } else { echo ""; } ?>';
var isAdmin = <?php  if(isset($_SESSION['isAdmin'])) { echo $_SESSION['isAdmin']; } else { echo "false"; } ?>;
var appURL = '<?php echo APP_URL ?>';
</script>

	<script type="text/javascript" src="js/extJS/ext-all.js"></script>
	
	<?php if($_SESSION['isLogin'] == TRUE) { ?>
		<!-- Panels -->
		<script type="text/javascript" src="js/unimatrix/chat/Application.js"></script>
		<!-- Utilities -->
		<script type="text/javascript" src="js/unimatrix/base/util/notification.js"></script>
		<script type="text/javascript" src="js/unimatrix/base/util/Validators.js"></script>
	<?php } else { ?>	
		<script type="text/javascript" src="js/unimatrix/base/login.js"></script>	
	<?php } ?>
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="js/extJS/resources/css/ext-all.css">
	<link rel="stylesheet" type="text/css" href="css/app.css">
	<link rel="stylesheet" type="text/css" href="css/icons.css">
</head>
<body>
<?php if($_SESSION['isLogin'] == false){ ?>
	<div align="left">
		<p>
			<b>Welcome to the UsualChat v0.5</b><br>
			To Register, just type your username and desired password into the login form and click register.<br>
			You will be able to login afterwards.<br>
			<br>
		</p>
	</div>
<? } ?>
</body>
</html>
