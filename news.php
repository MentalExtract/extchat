<html>
<head>
	<style type="text/css">
		p{font-family:Arial,Helvetica,sans-serif;font-size:14px;}
	</style>
</head>
<body>
	<p>
		<b>features for next releases:</b><br>
		- better notifications<br>
		- send offline messages<br>
		- admin functions<br>
		<br>
		<b>v0.5:</b><br>
		- some admin functions implemented<br>
		- UI improvements<br>	
		<br>
		<b>v0.4:</b><br>
		- controllers for better MVC implementation<br>	
		- personal settings (only change password atm)<br>
		<br>
		<b>v0.3:</b><br>
		- friends<br>
		- fancy icons<br>
		<br>
		<b>v0.2:</b><br>
		- privat chat<br>
		- notifications<br>
		- some major refactorisation<br>
		<br>
		<b>v0.1:</b><br>
		- basic chat functions<br>
		- multiple chat rooms<br>
		- create and join channels<br>
		<br>
		<b>Known bugs:</b></b><br>
		- redirect don't work on Safari<br>
		  workaround: right-click - open in new window<br>
		- cannot kick safari users<br>
		- can send messages to offline users<br>
		- doesn't leave the last closed channel<br>
		- 2 strange JS errors after loading users in channel<br>
	</p>
</body>
</html>
