<?php

require_once("UserStates.php");

class Chat {

	private $db;
	
	function __construct($user,$db) {
		$this->db = $db;
		$this->db->executeQuery("update user set state = ".ONLINE." where name = '".$user."' and state < ".BANNED); // set current user online
	}
	
	public function quit($user){
		$this->db->executeQuery("delete from usersinchannel where userName = '".$user."' ");
	}
	
	public function refresh(){
		$this->db->executeQuery("delete from usersinchannel where userName in(select name from user where lastFetch < '".date('Y-m-d H:i:s', time()-10)."')"); // remove offline users from channels
		$this->db->executeQuery("update user set state = ".OFFLINE." where state < 99 and lastFetch < '".date('Y-m-d H:i:s', time()-10)."'"); // set offline
	}
	
	public function sendMessage($user,$channel,$message){
		$this->db->executeQuery("insert into messages(user, message, channel) values('".$user."','".$message."','".$channel."')");
	} 
	public function getMessages($user, $lastFetch){
		$currentTime = date('Y-m-d H:i:s', time());
		$result = $this->db->executeQuery("select * from messages where time >= '".$lastFetch."' and time < '".$currentTime."' and (channel in (select channelName from usersinchannel where userName = '".$user."') or channel = '".$user."' or channel = 'GLOBAL') order by time");
		$messages = "{ \"messages\":".$this->db->getJsonResult($result)."}";
		$this->db->executeQuery("update user set lastFetch = '".$currentTime."' where name = '".$user."'");
		return $messages;		
	} 
	
	public function getChannels(){
		$result = $this->db->executeQuery("select c.name as channelName, (select count(userName) from usersinchannel uc where uc.channelName=c.name) userCount from channel c");
		return "{ \"success\":true,\"message\":\"loaded data\", \"data\":".$this->db->getJsonResult($result)."}";
	}
	
	public function createChannel($user,$channel){
		$this->db->executeQuery("insert into channel(name) values('".$channel."')");
		//$this->db->executeQuery("insert into usersinchannel(userName,channelName) values('".$user."','".$channel."')");
	}
	
	public function joinChannel($user,$channel){
		$this->db->executeQuery("insert into usersinchannel(userName,channelName) values('".$user."','".$channel."')");
	}
	
	public function leaveChannel($user,$channel){
		$this->db->executeQuery("delete from usersinchannel where userName = '".$user."' and channelName = '".$channel."'");
	}
	
	public function removeChannel($channel){
		$this->db->executeQuery("delete from usersinchannel where channelName = '".$channel."'");
		$this->db->executeQuery("delete from channel where name = '".$channel."'");
	}
	
	public function getUsersFromChannel($channel){		
		$result = $this->db->executeQuery("select u.id, u.name, IF( u.state = ".ONLINE.", 'images/online.png' , 'images/offline.png') as state from user u, usersinchannel uic where u.name = uic.userName and uic.channelName = '".$channel."' order by u.state DESC, u.name ASC");
		return "{ \"success\":true,\"message\":\"loaded data\", \"data\":".$this->db->getJsonResult($result)."}";
	}
	
	public function addFriend($user, $friendUser){
		$this->db->executeQuery("insert into friends(user, friendUser) values('".$user."','".$friendUser."')");
	}
	public function removeFriend($user, $friendUser){
		$this->db->executeQuery("delete from friends where user = '".$user."' and friendUser = '".$friendUser."'");
	}
	
	public function getFriends($user){
		$result = $this->db->executeQuery("select u.id, u.name, IF( u.state = ".ONLINE.", 'images/online.png' , 'images/offline.png') as state from user u, friends f where f.friendUser = u.name and f.user = '".$user."' order by u.state DESC, u.name ASC");
		return "{ \"success\":true,\"message\":\"loaded data\", \"data\":".$this->db->getJsonResult($result)."}";
	}
	
}
?>