<?php
class TreeNode {
 
	    public $text = "";
	    public $id = "";
	    public $qualifier = "";
	    public $iconCls = "";
	    public $leaf = true;
	    public $draggable = false;
	    public $href = "#";
	    public $hrefTarget = "";
	    public $expanded = true;
		
	 
	    function  __construct($id,$text,$qualifier,$iconCls,$leaf,$draggable,
	            $href,$hrefTarget, $expanded) {
	 
	        $this->id = $id;
	        $this->text = $text;
	        $this->qualifier = $qualifier;
	        $this->iconCls = $iconCls;
	        $this->leaf = $leaf;
	        $this->draggable = $draggable;
	        $this->href = $href;
	        $this->hrefTarget = $hrefTarget;
	        $this->expanded = $expanded;
	    }
	}
	 
	class TreeNodes {
	 
	    protected $nodes = array();
	 
	    function add($id,$text,$qualifier,$iconCls,$leaf,$draggable,
	        $href,$hrefTarget,$expanded) {
	 
	        $n = new TreeNode($id,$text,$qualifier,$iconCls,$leaf,
	                $draggable,$href,$hrefTarget,$expanded);
	 
	        $this->nodes[] = $n;
	    }
	 
	    function toJson() {
	        return json_encode($this->nodes);
	    }
	}
?>