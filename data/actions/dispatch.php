<?php
/**
 * Script to handle a chat session
 */

include($_SERVER['DOCUMENT_ROOT']."/chat/config/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/chat/data/classes/db/MySQLDB.php");
include($_SERVER['DOCUMENT_ROOT']."/chat/data/classes/app/ApplicationSession.php");
include($_SERVER['DOCUMENT_ROOT']."/chat/data/classes/app/Chat.php");

$app = new ApplicationSession();
$result = "";

if(isset($_REQUEST['action'])){

	if($_REQUEST['action'] != "register" && $_REQUEST['action'] != "login"){
		$chat = new Chat($app->getCurrentUser(),$app->getDB());
	}
	
	switch($_REQUEST['action']){
		
		/*
		 * user methods
		 */
		case 'login':
			$result = $app->login($_POST['userName'], $_POST['password']);
			break;
			
		case 'register':
			$result = $app->register($_POST['userName'], $_POST['password']);
			break;
			
		case 'logout':
			$chat->quit($app->getCurrentUser());
			$app->logout();
			break;
			
		case 'changeLoginData':
			if(isset($_POST['newPassword']) && $_POST['newPassword'] != ""){
				$app->changeLoginData($_POST['userName'], $_POST['newPassword']);
			}
			break;
		
		case 'banUser': 
			$result = $app->banUser($_POST['userName']);
			break;
		case 'unbanUser': 
			$result = $app->unbanUser($_POST['userName']);
			break;			
		case 'gotBanned':
			session_destroy();
			break;
					
		/*
		 * chat methods
		 */
		case 'getMessages':		
			$result = $chat->getMessages($app->getCurrentUser(),$app->getLastContact());
			$chat->refresh();
			$app->updateLastContact();
			break;
			
		case 'sendMessage':
			$result = $chat->sendMessage($app->getCurrentUser(), $_POST['channel'], strip_tags ($_POST['message']));
			break;	

		/*
		 * channel methods
		 */
		case 'getUsers':
			if(isset($_SESSION['currentChannel'])){
				$result = $chat->getUsersFromChannel($_SESSION['currentChannel']);				
			} 
			break;
		
		case 'getChannels':
			$result = $chat->getChannels();
			break;
		
		case 'createChannel':
			$chat->createChannel($app->getCurrentUser(), $_POST['channelName']);
			break;
		case 'joinChannel':
			$chat->joinChannel($app->getCurrentUser(), $_POST['channelName']);
			$_SESSION['currentChannel'] = $_POST['channelName'];
			break;
			
		case 'leaveChannel':
			$chat->leaveChannel($app->getCurrentUser(), $_POST['channelName']);
			break;
			
		case 'removeChannel':
			$chat->removeChannel($_POST['channelName']);
			break;			
			
		case 'setCurrentChannel':
			$_SESSION['currentChannel'] = $_POST['currentChannel'];
			break;

		/*
		 * friends methods
		 */	
		case 'addFriend':
			if($app->getCurrentUser() != $_POST['friendUser']){
				$chat->addFriend($app->getCurrentUser(), $_REQUEST['friendUser']);	
			}
			break;
			
		case 'removeFriend':
			$chat->removeFriend($app->getCurrentUser(), $_POST['friendUser']);
			break;

		case 'getMyFriends':
			$result = $chat->getFriends($app->getCurrentUser());
			break;
		case 'getFriendsOfUser':
			$result = $chat->getFriends($_REQUEST['user']);
			break;						
	}

}

echo $result;

?>