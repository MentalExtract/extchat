
Ext.onReady(function(){
  
	// workaround for safary bullshit
	var redirectIndex = function(){
		var redirect = appURL+'index.php'; 
    	window.location = redirect;
	};
	
  	var submitLogin = function(){
  		login.getForm().submit({ 
                        method:'POST', 
                        waitTitle:'Connecting', 
                        waitMsg:'Sending data...',
                        params:{
                    		action: 'login'
                    	},
                        success:function(){ 
                        	/*Ext.Msg.alert('Status', 'Login successful!', function(btn, text){
                        		if (btn == 'ok'){
                        			var redirect = 'http://localhost/page/index.php'; 
		                        	window.location = redirect;
                                 }
                        	});*/
                    		window.setTimeout(redirectIndex, 1);
                        },
                        failure:function(form, action){ 
                            if(action.failureType == 'server'){ 
                                obj = Ext.decode(action.response.responseText); 
                                Ext.Msg.alert('Login failed!', obj.error);
                            }else{ 
                                Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText); 
                            } 
                            login.getForm().reset(); 
                        } 
                    }); 
  	};
  	
    var login = new Ext.FormPanel({ 
        labelWidth:80,
        url: appURL+'data/actions/dispatch.php',
        frame:true, 
        //title:'Please Login', 
        defaultType:'textfield',
        monitorValid:true,
        items:[{ 
                fieldLabel:'Username', 
                name:'userName', 
                allowBlank:false 
            },{ 
                fieldLabel:'Password', 
                name:'password', 
                inputType:'password', 
                allowBlank:false,
                listeners:{
	        	   specialkey: function(f,e){
	                       if (e.getKey() == e.ENTER) {
	                       		submitLogin();
	                       }
                     }
           		}
            }],
	     
        buttons:[{ 
                text:'Login',
                formBind: true,	 
                handler:function(){ 
                    submitLogin();
                } 
            },
            {
            	text: 'Register',
            	formBind: true,	 
                 
                handler: function(){ 
            		
            		login.getForm().submit({ 
            			url: appURL+'data/actions/dispatch.php',
            			method:'POST', 
                        waitTitle:'Connecting', 
                        waitMsg:'Sending data...',
                        params:{
                    		action: 'register'
                    	},
                        success:function(){ 
                        	Ext.Msg.alert('Status', 'Register successful! You may now log in', function(btn, text){
                        		if (btn == 'ok'){
                        			//var redirect = 'http://localhost/page/index.php'; 
		                        	//window.location = redirect;
                                 }
                        	});
                        },
                        failure:function(form, action){ 
                            if(action.failureType == 'server'){ 
                                obj = Ext.decode(action.response.responseText); 
                                Ext.Msg.alert('Registration failed!', obj.error);
                            }else{ 
                                Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText); 
                            } 
                            login.getForm().reset(); 
                        } 
            		});
            		
            	}
            }] 
    });
 
    var win = new Ext.Window({
    	title: 'Login',
        layout:'fit',
        width:300,
        height:120,
        closable: false,
        resizable: false,
        plain: true,
        border: false,
        items: [login]
	});
	win.show();
});