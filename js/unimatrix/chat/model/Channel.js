Ext.define('unimatrix.chat.model.Channel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'channelName',   type: 'string'},
        {name: 'userCount',     type: 'number'}
    ]
});
