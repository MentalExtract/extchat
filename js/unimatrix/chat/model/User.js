Ext.define('unimatrix.chat.model.User', {
	extend: 'Ext.data.Model',
	fields: [
         {name: 'id',   	type: 'number'},
         {name: 'name',   	type: 'string'},
         {name: 'state',    type: 'string'}
     ]
});