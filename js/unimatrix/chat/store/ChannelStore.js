Ext.define('unimatrix.chat.store.ChannelStore',{
	extend: 'Ext.data.Store',
	singleton: true,
	requires  : ['unimatrix.chat.model.Channel'],
    model: 'unimatrix.chat.model.Channel',
    proxy: {
        type: 'ajax',
        url: appURL+'data/actions/dispatch.php',
        reader: Ext.create('Ext.data.JsonReader',{
        	successProperty: 'success',
        	idProperty: 'id',
        	root: 'data',
        	messageProperty: 'message'
        })
    }
});