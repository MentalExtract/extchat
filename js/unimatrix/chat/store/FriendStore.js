Ext.define('unimatrix.chat.store.FriendStore',{
	extend: 'Ext.data.Store',
	singleton: true,
	requires  : ['unimatrix.chat.model.User'],
    model: 'unimatrix.chat.model.User',
    proxy: {
        type: 'ajax',
        url: appURL+'data/actions/dispatch.php',
        reader: Ext.create('Ext.data.JsonReader',{
        	successProperty: 'success',
        	idProperty: 'id',
        	root: 'data',
        	messageProperty: 'message'
        })
    }
});