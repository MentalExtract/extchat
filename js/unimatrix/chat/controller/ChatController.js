/**
 * ChatController serves all required methods for calling the data operations via ajax from the UI
 */

Ext.define('unimatrix.chat.controller.ChatController',{
	extend:'unimatrix.chat.controller.BaseController',
	singleton: true,

	/**
	 * send a message to the specified channel. the channel also may be a user
	 * @param channel
	 * @param message
	 * @returns
	 */
	sendMessage: function(channel, message){
		this.superclass.dispatch({
		   	action:	'sendMessage',
		    message: message,
		    channel: channel
		});
	},

	/**
	 * creates a channel and reloads the channel store
	 * @param name
	 * @returns
	 */
	createChannel: function(name){
		this.superclass.dispatch({
	    		action: 'createChannel',
		        channelName: name
		    }, 
		    function(){
		    	unimatrix.chat.store.ChannelStore.load({
					params: {
						action : 'getChannels'
					}
			});	
		});
	},
	
	removeChannel: function(name){
		this.superclass.dispatch({
    		action: 'removeChannel',
	        channelName: name
	    }, 
	    function(){
	    	
	    	unimatrix.chat.store.ChannelStore.load({
				params: {
					action : 'getChannels'
				}
	    	});	
	    });
	},
	/**
	 * joins the specified channel and reloads the channel and user store
	 * @param channel
	 * @returns
	 */
	joinChannel: function(channel){
		
		this.sendMessage(channel, "/join "+currentUserName);
				
		this.superclass.dispatch({
	    		action: 'joinChannel',
		        channelName: channel
		    },
		    // reload the stores
		    function(){
		    	
				if(Ext.getCmp(channel) == null){
					var newChannel = Ext.create('unimatrix.chat.view.MessageForm',{id:channel,title:channel});//new MessageForm({id:name,title:name,closable:true});
					Ext.getCmp('contentPanel').add(newChannel);
				} else {
					var newChannel = Ext.getCmp(channel);
				}
		
				Ext.getCmp('contentPanel').setActiveTab(channel);	
		    	
		    	unimatrix.chat.store.ChannelStore.load({
						params: {
						action : 'getChannels'
					}
				});
		    	unimatrix.chat.store.UserStore.load({
					params:{
						action: 'getUsers'
					}
				});	
		    }
		);

	},
	
	/**
	 * leaves the specified channel
	 * @param channel
	 * @returns
	 */
	leaveChannel: function(channel){
		this.sendMessage(channel, "/leave "+currentUserName);
		this.superclass.dispatch({
    		action: 'leaveChannel',
	        channelName: channel
	    });
	},

	/**
	 * set the current active channel of the user on the server side and reload the stores
	 * @param channel
	 * @returns
	 */
	switchChannel: function(channel){
		this.superclass.dispatch({
	    		action: 'setCurrentChannel',
	    		currentChannel: channel
		    },
		    function() {
		    	unimatrix.chat.store.ChannelStore.load({
 						params: {
 						action : 'getChannels'
 					}
 				});
		    	unimatrix.chat.store.UserStore.load({
 					params:{
 						action: 'getUsers'
 					}
 				});	
		    }
		);
	},
	
	/**
	 * add a user as friend
	 * @param name
	 * @returns
	 */
	addFriend: function(name){
		this.superclass.dispatch({
	    		action: 'addFriend',
		        friendUser: name
		    },
		    function() {
				unimatrix.chat.store.FriendStore.load({
					params: {
						action : 'getMyFriends'
					}
				});
		    }
		);
	},
	
	/**
	 * removes a user from friends
	 * @param name
	 * @returns
	 */
	removeFriend: function(name){
		this.superclass.dispatch({
	    		action: 'removeFriend',
		        friendUser: name
		    },
		    function() {
				unimatrix.chat.store.FriendStore.load({
					params: {
						action : 'getMyFriends'
					}
				});
		    }
		);
	}
	
});