Ext.define('unimatrix.chat.controller.CommandController',{
	singleton: true,
	
	dispatchMessage: function(time, user, channel, message){
		var outMsg = '';
		var msgType = '';
		var cmd = '';
		var param = '';
		
		
		
		if(message.indexOf('/') === 0){
			msgType = 'CMD';
			cmd = message.split(' ')[0];
			param = message.split(' ')[1];
			//alert(cmd +" "+param);
		}
		
		if(msgType == 'CMD'){
			switch(cmd){
				case "/join": 
					outMsg = '('+time+') >>> '+param+' joined the channel';
					break;
				case "/leave":
					outMsg = '('+time+') >>> '+param+' has left the channel';
					break;
				case "/ban":
					outMsg = '('+time+') >>> '+param+' has been banned';
					if(param == currentUserName){
						Ext.Msg.alert("You are banned!","Because fuck you, thats why",unimatrix.chat.controller.UserController.logout());
					}
					break;
				case "/unban":
					outMsg = '('+time+') >>> '+param+' has been unbanned';
					break;
				default:
					outMsg = '('+time+') >>> command '+cmd+' is not supported';
			}
		} else {
			var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		    message = message.replace(exp,"<a href='$1' target='_blank'>$1</a>"); 
		    var theTime = time.split(" ")[1];
			outMsg = '('+theTime+') <<font color="#0000A0"><b>'+user+'</b></font>> '+message;
		}
		

		return outMsg;
	}
	
});