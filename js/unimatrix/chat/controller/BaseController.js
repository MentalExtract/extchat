/**
 * Basic controller for ajax calls 
 */

Ext.define('unimatrix.chat.controller.BaseController',{
	
	/**
	 * ajax function to call the chat dispatcher on the server side
	 * @param parameters
	 * @param successCallback
	 * @returns
	 */
	dispatch: function(parameters, successCallback){
		Ext.Ajax.request({
		    url: appURL+'data/actions/dispatch.php',
		    params: parameters,
		    method: 'POST',
		    success: function(xhr) {
				if (successCallback && typeof(successCallback) === "function") {
					successCallback(xhr);
				}
		    }
		});
	}
	
});