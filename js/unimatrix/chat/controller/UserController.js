/**
 * ChatController serves all required methods for calling the data operations via ajax from the UI
 */
Ext.define('unimatrix.chat.controller.UserController',{
	extend:'unimatrix.chat.controller.BaseController',
	singleton: true,

	login: function(){
		// oh this sucks, will be refactored l8er
	},
	
	logout: function(){
		this.superclass.dispatch({
		   		action: 'logout'
	   		}, 
	   		function() {
	   			window.location.reload();
	   		}
	   	);
	},
	
	changeLoginData: function(username, password){
					
			this.superclass.dispatch({
				action: 'changeLoginData',
				userName: username,
				newPassword: password
			});
		
	},
	
	/**
	 * add a user as friend
	 * @param name
	 * @returns
	 */
	addFriend: function(name){
		this.superclass.dispatch({
	    		action: 'addFriend',
		        friendUser: name
		    },
		    function() {
				unimatrix.chat.store.FriendStore.load({
					params: {
						action : 'getMyFriends'
					}
				});
		    }
		);
	},
	
	/**
	 * removes a user from friends
	 * @param name
	 * @returns
	 */
	removeFriend: function(name){
		this.superclass.dispatch({
	    		action: 'removeFriend',
		        friendUser: name
		    },
		    function() {
				unimatrix.chat.store.FriendStore.load({
					params: {
						action : 'getMyFriends'
					}
				});
		    }
		);
	},
	
	banUser: function(name){
		this.superclass.dispatch({
		   		action: 'banUser',
		   		userName: name
	   		},
	   		function(xhr){
	   			if(isAdmin){	   				
	   				unimatrix.chat.controller.ChatController.sendMessage('GLOBAL','/ban '+name);
//	   				var jsonResponse = Ext.decode(xhr.responseText);
//	   				eval(jsonResponse.msg);
	   			}
	   		}
	   	);
	},
	unbanUser: function(name){
		this.superclass.dispatch({
		   		action: 'unbanUser',
		   		userName: name
	   		},
	   		function(xhr){
	   			if(isAdmin){	   				
	   				unimatrix.chat.controller.ChatController.sendMessage('GLOBAL','/unban '+name);
//	   				var jsonResponse = Ext.decode(xhr.responseText);
//	   				eval(jsonResponse.msg);
	   			}
	   		}
	   	);
	}
	
});