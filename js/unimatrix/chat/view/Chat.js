Ext.define('unimatrix.chat.view.Chat', {
	extend:'Ext.Viewport',
	cls: 'transparent',
	requires: [
	           // views
	           'unimatrix.chat.view.News',
	           'unimatrix.chat.view.ChannelList',
	           'unimatrix.chat.view.UserList',
	           'unimatrix.chat.view.FriendList',
	           'unimatrix.chat.view.user.Settings',
	           
	           // controllers
	           'unimatrix.chat.controller.ChatController',
	           'unimatrix.chat.controller.UserController',
	           'unimatrix.chat.controller.CommandController'],
	layout: {
	    type: 'border',
	    padding: 5
	},
	
	initComponent: function(config){
	    Ext.apply(this, config);
	    this.callParent(arguments);
	},
	
	loadDefaults: function(){
		unimatrix.chat.controller.ChatController.joinChannel('Lobby');
		//Ext.getCmp('channelList').collapse();
		Ext.getCmp('friendsList').collapse();
		//Ext.getCmp('contentPanel').setActiveTab(0);
	},
	
	items: [
	        new Ext.Panel({
	        	 title:'UsualChat',
	        	 region:'west',
	        	 iconCls:'group',
	        	 align:'center',
	             split:true,
	             collapsible: true,
	             //width: '30%',
	             width: 180,
	             layout:'accordion',
	             items: [Ext.create('unimatrix.chat.view.FriendList'),Ext.create('unimatrix.chat.view.ChannelList')],
	             buttons:[
				           {
			                   text: 'Settings',
			                   autoWidth: true,
			                   listeners: {
				        	   		click:function(){
				        	   			if(Ext.getCmp('settingsWindow') == undefined){
				        	   				Ext.create('unimatrix.chat.view.user.Settings').show();
				        	   			} else {
				        	   				Ext.getCmp('settingsWindow').show();
				        	   			}
				           			}
			                   }
				           },
				           {
			                   text: 'Logout',
			                   autoWidth: true,
			                   listeners: {
				        	   		click:function(){
				        	   			unimatrix.chat.controller.UserController.logout();
				           			}
			                   }
				           }
				]
	        }),
			//new Ext.TabPanel({
	        new Ext.Panel({
				id: 'chatsPanel',
				activeTab: 0, 
				layout: 'fit',
				region: 'east',
				collapsed: false,
				split: true,
				//width: '30%',
				width:180,
				layout: 'fit',
				deferredRender:false,
				items: [Ext.create('unimatrix.chat.view.UserList')/*,
				        Ext.create('unimatrix.chat.view.FriendList')*/]
		      }),
			 new Ext.TabPanel({
	    	        id: 'contentPanel',
	                region: 'center',
	                width: '60%',
	                activeTab: 0, 
	                layout: 'fit',
	                tabPosition: 'top',
                	deferredRender:false,
	                items: [Ext.create('unimatrix.chat.view.News')],
	                listeners: {
				 		tabchange: function(tabPanel, tab){
				 			if(tab.title != "News"){
				 				
				 				unimatrix.chat.controller.ChatController.switchChannel(tab.title);
				 				// if it's a private chat, hide userlist
				 				var cPanel = Ext.getCmp(tab.title);
				 				if(cPanel.getPrivateChat() == true){
				 					if(cPanel.getDockedItems().length == 0){				 						
				 						cPanel.addDocked(cPanel.getToolbar(tab.title));
				 						cPanel.doLayout();
				 					}
				 					Ext.getCmp("chatsPanel").hide();
				 				} else {
				 					Ext.getCmp("chatsPanel").show();
				 				}
				 				tab.scrollDown(tab.title);
				 			} else {
				 				Ext.getCmp("chatsPanel").hide();
				 			}
			 			}
			 		}
	          })
	]
});
