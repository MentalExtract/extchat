Ext.define('unimatrix.chat.view.News', {
	extend: 'Ext.panel.Panel',
	title: 'News',
	iconCls: 'news',
	html: '<iframe frameborder=0 width="100%" height="100%" src="'+appURL+'/news.php"></iframe>'
});