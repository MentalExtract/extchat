Ext.define('unimatrix.chat.view.UserList', {
	extend: 'Ext.grid.Panel',
	requires: ['unimatrix.chat.store.UserStore',
	           'unimatrix.chat.view.MessageForm'],
	//title: 'Users',
	//iconCls: 'group',
	frame: false,
	border: false,
	defaults: {
	  border: false
	},
	viewConfig: {
		  loadMask: false,
          stripeRows: true,
          listeners: {
              itemcontextmenu: function(view, rec, node, index, e) {
                  e.stopEvent();
                  var ctx = Ext.create('Ext.menu.Menu', {
                	    items: [
                	    		Ext.create('Ext.Action', {
                	    		    iconCls: 'add',
                	    		    text: 'Add friend',
                	    		    handler: function(widget, event) {
                	    				unimatrix.chat.controller.UserController.addFriend(rec.get('name'));
                	    			}     
                	    		}),
                	    		Ext.create('Ext.Action', {
                	    		    iconCls: 'delete',
                	    		    text: 'Ban user',
                	    		    hidden: isAdmin ? false : true,
                	    		    handler: function(widget, event) {
                	    				unimatrix.chat.controller.UserController.banUser(rec.get('name'));
                	    			}     
                	    		})
                	        ]
                  });
                  if(currentUserName != rec.get("name")){ // cannot add yourself as a friend ;)
                	  ctx.showAt(e.getXY());  
                  }
                  return false;
              }
          }
	},
	initComponent: function(){
		this.store = unimatrix.chat.store.UserStore;
		this.columns = this.buildColumns();
		this.callParent();
	},
	 buildColumns: function(){
		 return [
					{header: "", sortable: true, width:30, renderer:this.renderIcon, dataIndex: 'state'},
					{header: "Name", sortable: true, dataIndex: 'name'}
				];
	 },	
	 renderIcon: function(val){
		return '<img src="' + val + '"/>';
	},
	listeners : {
		
		// click event on user opens a new MessageForm for privat chat
		itemclick: function(dv, record, item, index, e) {
			if(record.get('name') != currentUserName){
				
				var contentPanel = Ext.getCmp('contentPanel');
				var cMessageForm = Ext.create('unimatrix.chat.view.MessageForm',{
					id: record.get('name'),
					title:	record.get('name'),
					closable: true,
					privateChat: true
				});
				contentPanel.add(cMessageForm);
				contentPanel.setActiveTab(cMessageForm);
				
				unimatrix.chat.controller.ChatController.joinChannel(record.get('name'));
			}

		}
	}

});
