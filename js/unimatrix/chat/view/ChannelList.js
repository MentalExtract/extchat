Ext.define('unimatrix.chat.view.ChannelList',{
	extend: 'Ext.grid.Panel', 
	requires: ['unimatrix.chat.store.ChannelStore',
	           'unimatrix.chat.store.UserStore'],
	title: 'Channels',
	iconCls:'chat',
	id:'channelList',
	frame: false,
	initComponent: function(){
		this.store = unimatrix.chat.store.ChannelStore;
		this.columns = this.buildColumns();
		this.callParent();
		this.refreshList();
	},
	border: false,
	defaults: {
	  border: false
	},
	viewConfig: {
		  loadMask: false, 
		  forceFit:true,
		  listeners: {
              itemcontextmenu: function(view, rec, node, index, e) {
                  e.stopEvent();
                  var ctx = Ext.create('Ext.menu.Menu', {
                	    items: [
                	    		Ext.create('Ext.Action', {
                	    		    iconCls: 'delete',
                	    		    text: 'Delete channel',
                	    		    hidden: isAdmin ? false : true,
                	    		    handler: function(widget, event) {
                	    				unimatrix.chat.controller.ChatController.removeChannel(rec.get('channelName'));
                	    			}     
                	    		})
                	        ]
                  });
                  ctx.showAt(e.getXY());  
                  return false;
              }
          }
	},
	refreshList: function(){
    	unimatrix.chat.store.ChannelStore.load({
			params: {
				action : 'getChannels'
			}
    	});
	},
	listeners: {
		itemclick: function(dv, record, item, index, e) {
			this.joinChannel(record.get('channelName'));
	 	}
	},
	 buildColumns: function(){
		 return [
					{header: "Name", sortable: true, width:140, dataIndex: 'channelName'},
					{header: "", sortable: true, width:20, dataIndex: 'userCount'}
				];
	 },
	 dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			border: false,
			defaults: {
			  border: false
			},
			items: [
					/*{
						xtype: 'button',
						//text: 'New',
						iconCls: 'refresh',
						listeners : {
								click : function(){
									unimatrix.chat.store.ChannelStore.load({
										params: {
											action : 'getChannels'
										}
									});
							}
						}
					},*/					
					{
						xtype: 'button',
						text: '',
						hidden: isAdmin ? false : true,
						iconCls: 'addChannel',
						listeners : {
							click : function(){
								Ext.MessageBox.prompt(
										'Add channel', 
										'Please enter the name of the channel:', 
										function(btn, str){
											if(btn == "ok"){
												unimatrix.chat.controller.ChatController.createChannel(str);	
											}
										}
								);
							}
						}
					}/*,
					{
						xtype: 'button',
						text: '',
						iconCls: 'joinChannel',
						listeners : {
							click : function(){
								Ext.MessageBox.prompt(
										'Join channel', 
										'Please enter the name of the channel:', 
										function(btn, str){
											if(btn == "ok"){
												//this.joinChannel(str);
											}
										}
								);
							}
						}
					}*/	
			]
		}],
		// joins the selected room, reloads channel and user lists 
		// and opens a chat panel
		joinChannel: function(name){
		 	unimatrix.chat.controller.ChatController.joinChannel(name);		    
		    //this.openChatPanel(name);	
		},
		// opens chat panel for the selected room
		openChatPanel: function(name){
			if(Ext.getCmp(name) == null){
				var newChannel = Ext.create('unimatrix.chat.view.MessageForm',{id:name,title:name});//new MessageForm({id:name,title:name,closable:true});
				Ext.getCmp('contentPanel').add(newChannel);
			} else {
				var newChannel = Ext.getCmp(name);
			}
			Ext.getCmp('contentPanel').setActiveTab(newChannel);		
		}
	
});

