Ext.define('unimatrix.chat.view.FriendList', {
	extend: 'Ext.grid.Panel',
	requires: ['unimatrix.chat.store.FriendStore',
	           'unimatrix.chat.view.MessageForm'],
	iconCls: 'star',
	title: 'Friends',
	id:'friendsList',
	frame: false,
	viewConfig: {
		  loadMask: false,
          stripeRows: true,
          listeners: {
              itemcontextmenu: function(view, rec, node, index, e) {
                  e.stopEvent();
                  var ctx = Ext.create('Ext.menu.Menu', {
                	    items: [
                	    		Ext.create('Ext.Action', {
                	    		    iconCls: 'remove',
                	    		    text: 'Remove friend',
                	    		    handler: function(widget, event) {
                	    				unimatrix.chat.controller.UserController.removeFriend(rec.get("name"));
                	    			}     
                	    		})
                	        ]
                  });
                  ctx.showAt(e.getXY());
                  return false;
              }
          }
	},
	initComponent: function(){
		this.store = unimatrix.chat.store.FriendStore;
		this.columns = this.buildColumns();
		this.callParent();
	},
	 buildColumns: function(){
		 return [
					{header: "", sortable: true, width:30, renderer:this.renderIcon, dataIndex: 'state'},
					{header: "Name", sortable: true, dataIndex: 'name'}
				];
	 },	
	 renderIcon: function(val){
		return '<img src="' + val + '"/>';
	},
	listeners : {
		// double click event on user opens a new MessageForm for privat chat
		itemclick: function(dv, record, item, index, e) {
			
			var contentPanel = Ext.getCmp('contentPanel');
			var cMessageForm = Ext.create('unimatrix.chat.view.MessageForm',{
				id: record.get('name'),
				title:	record.get('name'),
				closable: true,
				privateChat: true
			});
			contentPanel.add(cMessageForm);
			contentPanel.setActiveTab(cMessageForm);
			
			unimatrix.chat.controller.ChatController.joinChannel(record.get('name'));
		}
	}

});
