Ext.define('unimatrix.chat.view.MessageForm', {
	extend: 'Ext.FormPanel',
	frame: false,
	deferredRender: false,
	closable: true,
	iconCls: 'comment',
	padding: 0,
	config: {
		privateChat: false
	},
	initComponent: function(config) {
		this.callParent();
	},
	addMessage: function(to, msg) {
		//var textArea = Ext.getCmp(to).getEl().down('textarea');
		var textArea = Ext.getCmp(to).getEl().down('div.msgBox');
		var outMsg = unimatrix.chat.controller.CommandController.dispatchMessage(msg.time, msg.user, this.title, msg.message);
		//		textArea.dom.value=
		//			textArea.getValue()+"\n"
		//			+outMsg;
		Ext.DomHelper.append(textArea, {
			cn: outMsg
		});
		this.scrollDown(to);
	},
	scrollDown: function(cmp) {
		//var textArea = Ext.getCmp(cmp).getEl().down('textarea');
		var textArea = Ext.getCmp(cmp).getEl().down('div.msgBox');
		textArea.dom.scrollTop = textArea.dom.scrollHeight;
	},
	listeners: {
		// close event triggers leaveChannel action on server side
		close: function() {
			unimatrix.chat.controller.ChatController.leaveChannel(this.title);
		}
	},
	getToolbar: function(extUser) {
		return [{
			xtype: 'toolbar',
			dock: 'top',
			//rendered: privateChat,
			items: [
				{
				xtype: 'button',
				text: 'Add as friend',
				iconCls: 'online',
				listeners: {
					click: function() {
						unimatrix.chat.controller.ChatController.addFriend(extUser);
					}
				}
			},
				{
				xtype: 'button',
				text: 'Ban user',
				iconCls: 'delete',
				hidden: isAdmin ? false : true,
				listeners: {
					click: function() {
						unimatrix.chat.controller.UserController.banUser(extUser);
					}
				}
			}

			]
		}];
	},
	items: [
		{
		//xtype: 'textareafield',
		xtype: 'component',
		autoScroll: 'true',
		cls: 'msgBox',
		anchor: '0 95%'
	},
		{
		xtype: 'textfield',
		anchor: '0 5%',
		listeners: {
			specialkey: function(f, e) {
				if (e.getKey() === e.ENTER) {
					if (isAdmin && f.value.indexOf('/') === 0) {
						unimatrix.chat.controller.ChatController.sendMessage(this.up("[xtype='form']").title, f.value);
					} else if (!isAdmin && f.value.indexOf('/') === 0) {

					} else if (f.value !== '') {
						unimatrix.chat.controller.ChatController.sendMessage(this.up("[xtype='form']").title, f.value);
					}
					this.setValue();
				}
			}
		}
	}
	]
});