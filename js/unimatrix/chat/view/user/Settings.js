Ext.define('unimatrix.chat.view.user.Settings', {
	extend: 'Ext.window.Window',
	
	id: 'settingsWindow',
	title: 'Settings',

	layout:'fit',
    width:450,
    //height:300,
    //closable: true,
    closeAction:'hide',
    resizable: false,
    plain: true,
    border: false,
    draggable: false,
	closeAction: 'hide',
		
	items: [
	        
	    new Ext.FormPanel({
	    	id: 'settingsForm',
	    	bodyStyle:'padding:15px',
	    	frame: true,
		    fieldDefaults: {
			    labelAlign: 'left',
			    labelWidth: 200,
			    anchor: '100%'
			},
			items: [{
		        // Fieldset 1
		        xtype:'fieldset',
		        columnWidth: 0.5,
		        title: 'Login data',
		        //collapsible: true,
		        defaultType: 'textfield',
		        defaults: {anchor: '100%'},
		        layout: 'anchor',
			items: [{
				id: 'userName',
		        xtype: 'textfield',
		        name: 'userName',
		        fieldLabel: 'Username',
		        disabled: true,
		        value: currentUserName,
		        allowBlank:false
		     
		    },{
		    	id: 'newPassword',
		        xtype: 'textfield',
		        name: 'newPassword',
		        inputType: 'password',
		        fieldLabel: 'New password',
		        allowBlank:true
		     },{
		        xtype: 'textfield',
		        name: 'password-cnf',
		        inputType: 'password',
		        fieldLabel: 'Confirm password',
                vtype: 'password',
                initialPassField: 'newPassword', // id of the initial password field
		        allowBlank:true
			 }]
			}/*,{
		        // Fieldset 2
		        xtype:'fieldset',
		        columnWidth: 0.5,
		        title: 'Personal data',
		        //collapsible: true,
		        defaultType: 'textfield',
		        defaults: {anchor: '100%'},
		        layout: 'anchor',
				items: [{
					id: 'email',
			        xtype: 'textfield',
			        name: 'emailField',
			        fieldLabel: 'E-Mail',
			        allowBlank:true
			     
			    },{
					id: 'birthday',
			        xtype: 'datefield',
			        name: 'birthdayField',
			        fieldLabel: 'Birthday',
			        allowBlank:true
			     
			    },{
					id: 'city',
			        xtype: 'textfield',
			        name: 'cityField',
			        fieldLabel: 'City',
			        allowBlank:true
			     
			    }]
			}*/],
			 buttons: [{
	                text:'Save',
	                ref: 'saveButton',
	                formBind: true,	 
	                handler:function(){
				 		currentUserName = Ext.getCmp('userName').getValue();
				 		unimatrix.chat.controller.UserController.changeLoginData(
				 				currentUserName,
				 				Ext.getCmp('newPassword').getValue()
				 		);
					 	Ext.getCmp('settingsForm').getForm().reset();
					 	Ext.getCmp('settingsWindow').hide();
	                } 
				 },{
	                text:'Abort',
	                handler:function(){
					 	Ext.getCmp('settingsForm').getForm().reset();
					 	Ext.getCmp('settingsWindow').hide();
					 	
	                } 
				 }]
	    })
	]
});