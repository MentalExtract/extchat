/**
 * MessageThread fetchs all new messages from the server
 * and appends them to the MessageForm
 * interval: 1 second
 */ 

Ext.define('unimatrix.chat.threads.MessageThread',{
	run: function(){
        Ext.Ajax.request({
        	url: appURL+'data/actions/dispatch.php',
    	    method: 'POST',
    	    params: {
        		action: 'getMessages'
        	},

    	    success: function(xhr) {
        		var cChannel;
    	        var jsonResponse = Ext.decode(xhr.responseText);
    	        Ext.each(jsonResponse.messages, function(cMsg){
    	        	cChannel = cMsg.channel;
    	        	if(cChannel != null && cMsg.message.substring(0,1) != '/'){

        	        	// incoming private message
        	        	if(cChannel == currentUserName){
        	        		var newMessageForm;
        	        		if(Ext.getCmp(cMsg.user) == null){
        	        			newMessageForm =  Ext.create('unimatrix.chat.view.MessageForm',{
        		                	id: cMsg.user,
        		                	title: cMsg.user,
        		                	closable: true,
        		                	deferredRender:false,
        		                	privateChat: true
        		                });
            	        		Ext.getCmp('contentPanel').add(newMessageForm);

            	        		// workaround for deferredRender:false
            	        		var a = Ext.getCmp('contentPanel').getActiveTab();
            	        		var idx = Ext.getCmp('contentPanel').items.indexOf(a);
            	        		
            	        		Ext.getCmp('contentPanel').setActiveTab(newMessageForm);
            	        		Ext.getCmp('contentPanel').setActiveTab(idx);
            	        		
            	        		//if(cMsg.message.substring(0,1) != '/'){  
	            	        		// join channel on server side
	            	        		Ext.Ajax.request({
	            	    			    url: appURL+'data/actions/dispatch.php',
	            	    			    params: {
	            	    		    		action: 'joinChannel',
	            	    			        channelName: cMsg.user
	            	    			    },
	            	    			    method: 'POST',
	            	    			    success: function(xhr) {
	            	    			    }
	            	    			});
            	        		   	        			
            	        			// notify
            	        			Ext.notification.msg(cMsg.user+' sent you a message!', cMsg.message.substring(0,40)+'...');
            	        		//}
        	        		}  else {
        	        			newMessageForm = Ext.getCmp(cMsg.user);
        	        		}
            	        	// append message to privat channel           	        			
        	        		newMessageForm.addMessage(cMsg.user, cMsg);

        	        	} else { // income message on public channel
            	        	// append message to channel
        	        		if(typeof(Ext.getCmp(cChannel))!= 'undefined'){
        	        			Ext.getCmp(cChannel).addMessage(cChannel, cMsg);
        	        		} else {
        	        			
        	        		}
        	        	}
        	        	

    	        		
    	        	}
    	       }, this);
    	        

    	    }
    	});
	    
	},
	interval: 1000
});