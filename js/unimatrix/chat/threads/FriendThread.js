/**
 * UserThread reloads the UserStore
 * interval: 10 seconds
 */

Ext.define('unimatrix.chat.threads.FriendThread',{
	requires: ['unimatrix.chat.store.FriendStore'],
    run: function(){
		unimatrix.chat.store.FriendStore.load({
			params: {
				action : 'getMyFriends'
			}
		});
	},
	interval: 15000
});