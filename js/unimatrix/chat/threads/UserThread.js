/**
 * UserThread reloads the UserStore
 * interval: 10 seconds
 */

Ext.define('unimatrix.chat.threads.UserThread',{
    run: function(){
		unimatrix.chat.store.UserStore.load({
			params: {
				action : 'getUsers'
			}
		});
	},
	interval: 10000
});