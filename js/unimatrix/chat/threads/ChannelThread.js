/**
 * ChannelThread reloads the ChannelStore 
 * interval: 5 seconds
 */
Ext.define('unimatrix.chat.threads.ChannelThread',{
	//requires: ['unimatrix.chat.store.ChannelStore'],
    run: function(){
		unimatrix.chat.store.ChannelStore.load({
			params: {
				action : 'getChannels'
			}
		});
	},
	interval: 5000
});