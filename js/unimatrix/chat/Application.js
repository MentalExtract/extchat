Ext.Loader.setConfig({
    enabled : true,
    paths   : {
        unimatrix : 'js/unimatrix'
    } 
});

Ext.require('unimatrix.chat.view.Chat');

Ext.define('unimatrix.chat.Application',{
	
	runner:  new Ext.util.TaskRunner(),

	initComponent: function(){
		this.callParent();
	},

	loadUI: function(){
		var chatApp = Ext.create('unimatrix.chat.view.Chat');
		chatApp.loadDefaults();
		
	},
	startThreads: function(){
		this.runner.start(Ext.create('unimatrix.chat.threads.ChannelThread'));
		this.runner.start(Ext.create('unimatrix.chat.threads.FriendThread'));
		this.runner.start(Ext.create('unimatrix.chat.threads.UserThread'));
		this.runner.start(Ext.create('unimatrix.chat.threads.MessageThread'));	
	}
});

Ext.onReady(function() {
	
	applyValidators();
	
	var chat = Ext.create('unimatrix.chat.Application');
	chat.loadUI();
	chat.startThreads();

});